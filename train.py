from netpy.teachers import BackPropTeacher
from netpy.nets import FeedForwardNet
import numpy as np
import math as m
import logging
from os import listdir

net = FeedForwardNet(name = 'ocean_oc')

net.load_net_data()
net.load_weights()

teacher = BackPropTeacher(net,
                          error = 'MSE')

logging.basicConfig(filename='ocean_oc_data/ocean_oc.log', level=logging.INFO)

while True:

    dir_X = listdir('/data/learning/X/')
    dir_Y = listdir('/data/learning/Y/')

    dir_X = dir_X[::-1]
    dir_Y = dir_Y[::-1]


    for i in range(len(dir_X)):

        print('===================================================================')
        print(str(dir_X[i]), str(dir_Y[i]))
        print('===================================================================')

        train_X = np.loadtxt('/data/learning/X/'+str(dir_X[i]), delimiter=',')
        train_Y = np.loadtxt('/data/learning/Y/'+str(dir_Y[i]), delimiter=',')

        eps = 0
        sigma = 0
        N = 1000
        y_min = 100
        y_max = -50
        y_tmp = 1
        sigma_y = 0
        num = 1
        num_of_epoch = len(train_X)

        for i in range(len(train_X)):

            N = i + 1

            x1 = []
            x1.append(train_X[i])
            y1 = []
            y1.append(train_Y[i])

            if y_min > train_Y[i]:
                y_min = train_Y[i]

            if y_max < train_Y[i]:
                y_max = train_Y[i]


            teacher.train(x1, y1,
                        1,
                        random_data = False,
                        logging = False,
                        learning_rate = [0.001, 0.002])

            y_tmp = (y_tmp*N+train_Y[i])/(N+1)
            eps_tmp = net.loss/(y_tmp)
            eps = (eps*N+eps_tmp)/(N+1)

            sigma_y_tmp = y_tmp - train_Y[i]
            sigma_y = m.sqrt((N*(sigma_y**2) + (sigma_y_tmp)**2)/(N+1))
            sigma_tmp = eps_tmp - eps
            sigma = m.sqrt((N*(sigma**2) + (sigma_tmp)**2)/(N+1))
            iq = (1 - eps)*100

            print(i, '{0:.4f}'.format(eps), '{0:.4f}'.format(sigma), "\t",'{0:.4f}'.format(iq),
                        '{0:.4f}'.format(y_tmp), '{0:.4f}'.format(sigma_y))

        logging.info("%s | Eps: %s | Sigma: %s | Iq: %s | Y_tmp: %s | Sigma_Y: %s " %
                    (i, '{0:.5f}'.format(eps), '{0:.5f}'.format(sigma), '{0:.4f}'.format(iq),
                                    '{0:.4f}'.format(y_tmp), '{0:.4f}'.format(sigma_y)))                         
