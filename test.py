from netpy.nets import FeedForwardNet

net = FeedForwardNet(name = 'ocean_oc')

net.load_net_data()
net.load_weights()

print(net.forward(test))
